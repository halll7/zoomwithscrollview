//
//  ViewController.swift
//  zoomWithScrollView
//
//  Created by Leigh Hall on 17/05/2018.
//  Copyright © 2018 Leigh Hall. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    fileprivate var scrollView: UIScrollView!
    fileprivate var imageView: UIView!
    fileprivate var lockButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let testCardImage = UIImage(named: "testcard")
        imageView = UIImageView(image: testCardImage)
        
        scrollView = UIScrollView(frame: view.bounds)
        scrollView.backgroundColor = .black
        scrollView.contentSize = imageView.bounds.size
        scrollView.maximumZoomScale = 5.0
        view.addSubview(scrollView)

        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.fitInsideView(self.view, top: 100, bottom: -100, left: 0, right: 0)
        
        setMinZoomScale()
        centreContent()
        
        scrollView.delegate = self
        scrollView.addSubview(imageView)
        
        lockButton = UIButton(type: .system)
        lockButton.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lockButton)
        lockButton.setTitle("Lock", for: .normal)
        lockButton.addTarget(self, action: #selector(lockButtonTapped), for: .touchUpInside)
        lockButton.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -8).isActive = true
        lockButton.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -8).isActive = true
        lockButton.widthAnchor.constraint(equalToConstant: 120).isActive = true
        lockButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    override func viewDidLayoutSubviews() {
        setMinZoomScale()
        centreContent()
    }
    
    @objc func lockButtonTapped(_ tap: UIEvent) {
        let zoomEnabled = scrollView.isUserInteractionEnabled
        lockButton.setTitle(zoomEnabled ? "Unlock" : "Lock", for: .normal)
        scrollView.isUserInteractionEnabled = !zoomEnabled
    }
    
    private func setMinZoomScale() {
        let imageViewSize = imageView.bounds.size
        let scrollViewSize = scrollView.bounds.size
        let widthScale = scrollViewSize.width / imageViewSize.width
        let heightScale = scrollViewSize.height / imageViewSize.height
        
        scrollView.minimumZoomScale = max(widthScale, heightScale)
        scrollView.zoomScale = 1.0
    }

    private func centreContent() {
        let centerOffsetX = (scrollView.contentSize.width - scrollView.frame.size.width) / 2
        let centerOffsetY = (scrollView.contentSize.height - scrollView.frame.size.height) / 2
        let centerPoint = CGPoint(x: centerOffsetX, y: centerOffsetY)
        scrollView.setContentOffset(centerPoint, animated: true)
    }
    
}

extension ViewController: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
}

extension UIView {
    
    func fitInsideView(_ outer: UIView, top:CGFloat = 0, bottom:CGFloat = 0, left:CGFloat = 0, right:CGFloat = 0) {
        self.topAnchor.constraint(equalTo: outer.topAnchor, constant: top).isActive = true
        self.bottomAnchor.constraint(equalTo: outer.bottomAnchor, constant: bottom).isActive = true
        self.leadingAnchor.constraint(equalTo: outer.leadingAnchor, constant: left).isActive = true
        self.trailingAnchor.constraint(equalTo: outer.trailingAnchor, constant: right).isActive = true
    }
    
}

